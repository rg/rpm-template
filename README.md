# RPM template

This is a template of a repository for RPM building through pipeline.

## Get started

- Copy all your sources to `./sources/`, without the top directory `your_package_name` if you used to got one. Or set `./sources` as a git submodule to your sources
- Remove the `./specs/example.spec` file and copy your's file instead
- Test on some RHEL / CentOS 6 or 7 server, `make install` for dependencies once then `make` until it works
- Adapt your spec file if required
- Commit and check if the pipeline is fine.
- Uncomment upload in the pipeline

## Technical details

### Pipelines

There are pipeline definitions available (`.gitlab-ci.yml` and `Jenkinsfile` files), which are quite generic since all the scripting is in the Makefile. This for a portability reason: anyone should be able to manually build the RPM outside of the CI, on any RHEL/CentOS 6/7+ server. And this work can be used with any orchestrator, not just Gitlab CI or Jenkins.

### Makefile

The Makefile itself is meant to be generic too, so it can be copy/pasted. The big idea is to clean (previous build) then (rpm)build. Optionally upload (to Nexus OSS in this example). Plus initial clean just in case and install just for information or manual usage. `make` will execute target `all` by default which is `clean && build`.

### RPM

Nothing much to say except, an example is provided. Be careful about relative path whenever migrating here. For `example.spec`, I took source & spec from an old SVN and replaced some `%{name}` by `sources`, and did some cleaning and adjustement like replacing
  ```
  [ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
  install -d -m 0755 %{buildroot}/etc/cron.d/
  cp * %{buildroot}/etc/cron.d/
  ```
  by a single `cp -r * %{buildroot}` since the directory tree is now in my source instead of being created during %install.
