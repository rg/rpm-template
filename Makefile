.DEFAULT_GOAL := all

# The env vars should be defined in the env, most likely by the CI/CD tools
#    - https://gitlab/user-or-group/repository/settings/ci_cd
#    - Jenkins Credentials + credentials() function

all: clean rpm

install:
	# Generic tools for RPM building
	yum install -y autoconf automake curl dos2unix epel-release make mkisofs rpm-build rpmdevtools tar git subversion unzip

rpm:
	mkdir -p ~/rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
	tar -czf sources.tar.gz sources
	mv sources.tar.gz ~/rpmbuild/SOURCES
	rpmbuild --define "debug_package %{nil}" -bb specs/*.spec
	mv ~/rpmbuild/RPMS/*/*.rpm ./

upload:
	for r in $$(ls ./*.rpm); do curl --silent --fail --insecure --user $${UPLOAD_CREDS} --upload-file $$r $${UPLOAD_URL}/$$r; done

clean:
	rm -rf ~/rpmbuild/ ./*.rpm
