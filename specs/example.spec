#################### Specific ####################
%define name company-app-config
%define version 1.2.3
%define release 1

Summary:    company App Config
Requires:   cronie

#################### Generic ####################
Name:       %{name}
Version:    %{version}
Release:    %{release}%{?dist}
License:    Private
Group:      Application/System
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix:     %{_prefix}
BuildArch:  noarch
Vendor:     Company
Source0:    src.tar.gz

#################### Specific ####################
%description
This packages contains Company App config files.

%prep
%setup -n src

%install
cp -r * %{buildroot}

%post
service crond reload

%files
%defattr(644,root,root)
/etc/*
